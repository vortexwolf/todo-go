package models

// Task model
type Task struct {
	Text string
	Done bool
}
